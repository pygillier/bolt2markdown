"""Bolt2Markdown

This script allows you to export your bolt.com entries and pages
to markdown file consumable by pelican

Usage:
    export.py [options] tables
    export.py [options] export --table=<table>
    export.py (-h | --help)

Options:
    --config=<configfile> Path to config file to use  [default: example.ini]
    --verbose             Verbose mode
    -h|--help             Show this screen.

"""
import os
import logging
from docopt import docopt
from bolt2markdown.utils import init_logging
from bolt2markdown.core import Bolt2Markdown

logger = logging.getLogger(__name__)


def main(args):
    init_logging(args['--verbose'])

    # If config file location doesn't start with a "/", we consider it is
    # relative to current directory
    if not args['--config'].startswith('/'):
        config_location = '/'.join([os.getcwd(), args['--config']])
    else:
        config_location = args['--config']

    exporter = Bolt2Markdown(cfg_file=config_location)

    if args['tables'] is True:
        names = [table.name for table in exporter.list_available_tables()]
        logger.info("Available content tables: %s", ', '.join(names))

    if args['export'] is True:
        exporter.export_table(args['--table'])
        logger.info("Export ended for %s", args['--table'])


if __name__ == '__main__':
    arguments = docopt(__doc__, version='export.py 0.1')
    main(arguments)
