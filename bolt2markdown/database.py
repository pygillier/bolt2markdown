from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import logging


logger = logging.getLogger(__name__)


class Database:

    engine = None
    session = None
    prefix = None

    def __init__(self, config):

        dsn = "%(driver)s://%(username)s:%(password)s" \
              "@%(host)s:%(port)s/%(database)s" % {
                'driver': config['driver'],
                'username': config['username'],
                'password': config['password'],
                'host': config['host'],
                'port': config['port'],
                'database': config['database'],
                }
        logger.info("Using database: %s", dsn)

        self.prefix = config['table_prefix']
        self.engine = create_engine(dsn)

    def get_session(self):
        if self.session is None:
            my_session = sessionmaker(bind=self.engine)

            # create a Session
            self.session = my_session()

        return self.session

    def get_user(self, user_id):
        return "toto"
        return self.session.query('bolt_user').filter_by(id=user_id).first()
