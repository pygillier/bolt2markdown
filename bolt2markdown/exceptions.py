import logging

logger = logging.getLogger(__name__)


class TableNotFoundException(Exception):
    pass
