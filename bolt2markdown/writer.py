import logging
import os
from os.path import dirname, join
import html2markdown
logger = logging.getLogger(__name__)


class Writer:

    _build_root = None
    _absolute = False

    def __init__(self, build_root, absolute=False):

        if not absolute:
            self._build_root = join(dirname(dirname(__file__)), build_root)
        else:
            self._build_root = build_root

        logger.info("Using output base directory: %s", self._build_root)
        try:
            os.mkdir(self._build_root)
        except OSError:
            logger.warning("Output directory already exists")

    def write_file(self, directory, row):
        if not os.path.exists(os.path.join(self._build_root, directory)):
            logger.info("Creating %s output directory", directory)
            os.mkdir(os.path.join(self._build_root, directory))

        file_name = '.'.join([row.slug, 'md'])
        file_path = '/'.join([self._build_root, directory, file_name])
        logger.info("Writing %s", file_path)

        with open(file_path, 'w+') as f:
            f.writelines("Title: %s\n" % row.title)
            f.writelines("Date: %s\n" % row.datecreated)
            f.writelines("Modified: %s\n" % row.datechanged)
            f.writelines("Slug: %s\n" % row.slug)
            f.writelines("Author: %s\n" % row.ownerid)
            if row.teaser is not None:
                f.writelines("Summary: %s\n" % row.teaser)

            # Finally, write the content!
            f.writelines("\n\n%s" % html2markdown.convert(row.body))
