from configparser import ConfigParser
from .listing import Listing
from .database import Database
from .writer import Writer
from .utils import get_config_section
from .exceptions import TableNotFoundException
import logging

logger = logging.getLogger(__name__)


class Bolt2Markdown:

    _config = None
    _listing = None
    _database = None

    def __init__(self, cfg_file):

        self._init_config(cfg_file)
        self._init_database()
        self._listing = Listing(self._database)
        self._writer = Writer("build")

    def _init_config(self, cfg_file):
        if self._config is None:
            logger.info("Using cfg file: %s", cfg_file)
            self._config = ConfigParser()
            self._config.read(cfg_file)

    def _init_database(self):
        db_config = get_config_section(self._config, "database")
        self._database = Database(db_config)

    def list_available_tables(self):
        logger.debug("Calling Listing.get_tables")
        return self._listing.get_content_tables()

    def export_table(self, table_name):
        logger.debug("Exporting table %s", table_name)

        # Get the table from available ones
        table_to_export = next(
            (
                table for table in self.list_available_tables()
                if table.name == table_name
            ),
            None
        )

        if table_to_export is None:
            logger.error("Table not found in database: %s", table_name)
            raise TableNotFoundException(table_name)

        # Define export folder name
        export_folder = table_name.replace(self._database.prefix, "")
        logger.info("Exporting in %s", export_folder)

        session = self._database.get_session()
        results = session.query(table_to_export).all()

        for result in results:
            # user = self._database.get_user(result.ownerid)
            self._writer.write_file(export_folder, result)
