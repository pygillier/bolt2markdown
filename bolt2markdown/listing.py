"""Listing class"""
import logging
from sqlalchemy import MetaData

logger = logging.getLogger(__name__)


class Listing:

    """Internal bolt tables"""
    _internal_tables = [
        'bolt_authtoken',
        'bolt_log',
        'bolt_users',
        'bolt_taxonomy',
        'bolt_log_change',
        'bolt_log_system',
        'bolt_cron',
        'bolt_content_changelog',
        'bolt_field_value'
    ]

    def __init__(self, database):
        logger.debug("Init Listing class")
        self._database = database

    def get_internal_tables(self):
        return self._internal_tables

    def get_content_tables(self):
        logger.debug("Listing content tables")
        meta = MetaData()
        meta.reflect(bind=self._database.engine)

        tables = [table for table in meta.sorted_tables
                  if table.name not in self.get_internal_tables()]
        return tables
