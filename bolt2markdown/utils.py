import logging

logger = logging.getLogger(__name__)


def init_logging(verbose=False):
    if verbose:
        logging.basicConfig(
            level=logging.DEBUG,
            format='%(asctime)s - %(levelname)-8s - %(name)s - %(message)s'
        )
        logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
    else:
        logging.basicConfig(
            level=logging.INFO,
            format='%(asctime)s - %(levelname)-8s - %(message)s'
        )


def get_config_section(config, section):
    dict1 = {}
    options = config.options(section)
    for option in options:
        try:
            dict1[option] = config.get(section, option)
            if dict1[option] == -1:
                logger.warning("skip: %s" % option)
        except Exception as e:
            logger.error("exception on %s!" % option)
            dict1[option] = None
    return dict1
